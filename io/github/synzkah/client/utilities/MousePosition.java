package io.github.synzkah.client.utilities;

import lombok.Getter;

public class MousePosition {
	
	@Getter private static Integer posX;
	@Getter private static Integer posY;
	
	public static void set(Integer X, Integer Y) {
		posX = X;
		posY = Y;
	}
}
