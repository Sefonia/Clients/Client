package io.github.synzkah.client.interfaces.games;

import java.util.List;

import com.google.common.collect.Lists;

import io.github.synzkah.client.components.Size;
import io.github.synzkah.client.components.buttons.Button;
import io.github.synzkah.client.components.buttons.actions.Action;
import io.github.synzkah.client.components.buttons.actions.DisplayScreenAction;
import io.github.synzkah.client.components.buttons.informations.EscapeButtonInformations;
import io.github.synzkah.client.components.buttons.informations.Informations;
import io.github.synzkah.client.components.buttons.type.IconType;
import io.github.synzkah.client.resources.Resource;
import lombok.Data;
import net.minecraft.client.Minecraft;

@Data
public class EscapeButtons {
	
	private List<Button> buttons = Lists.newArrayList();
	
	/**
	 * 
	 */
	
	private Size defaultSize = new Size(20, 20);
	
	public EscapeButtons() {
		
        Button home = new Button(7, 15);
        
        home.setSize(this.getDefaultSize());
        home.setType(new IconType(Resource.HOME));
        home.setInformations(new EscapeButtonInformations(9, 38));
        home.setTitle("Accueil");
        home.addAction(new DisplayScreenAction(null));
        
        home.enable();
        
        this.buttons.add(home);
		
	}
}
