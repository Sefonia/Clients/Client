package io.github.synzkah.client;

import java.lang.reflect.Modifier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.github.synzkah.client.notifications.NotificationsManager;
import io.github.synzkah.client.settings.Settings;
import io.github.synzkah.client.settings.SettingsManager;
import lombok.Getter;

/**
 * @author Synzkah
 *	23 jul. 2018
 */

public class Client {
	
	// TODO: Interface builder RTE (Real Time Editing component helpful for Building)
	// TODO: Liste des notifications (demande d'ami etc)
	
	/**
	 * Instance
	 */
	
	public static Client i;
	@Getter private Gson gson;
	
	/**
	 * Version
	 */
	
	public static String CLIENT_VERSION = "1.0.0-SNAPSHOT";
	public static String SUB_VERSION = "1.0.0";
	public static String VERSION = "SNAPSHOT";
	
	/**
	 * Settings
	 */
	
	private SettingsManager settingsManager;	
	@Getter private Settings settings;
	
	/**
	 * 
	 */
	
	@Getter private NotificationsManager notificationsManager;
	
	public Client() {
		
		i = this;
		gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls().excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE).create();
		
		/**
		 * Settings
		 */
		
		this.settingsManager = new SettingsManager();
		settings = settingsManager.getSettings();
		
		/**
		 * Load Notifications
		 */
		
		this.notificationsManager = new NotificationsManager();
		
		
	}
	
	public void shutdown() {
		
	}

}