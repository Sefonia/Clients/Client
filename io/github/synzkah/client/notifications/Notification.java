package io.github.synzkah.client.notifications;

import io.github.synzkah.client.notifications.enums.NotificationType;
import lombok.Data;

@Data
public abstract class Notification {
	
	private String id;
	private String title;
	private String content;
	private NotificationType type;

}