package io.github.synzkah.client.notifications;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;

public class NotificationsManager {
	
	public static NotificationsManager i;
	
	/**
	 * Notifications
	 */
	
	@Getter private List<Notification> notifications;
	
	public NotificationsManager() {
		
		i = this; 
		
		/**
		 * 
		 */
		
	}
	
	

}
