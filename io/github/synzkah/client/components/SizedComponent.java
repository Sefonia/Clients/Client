package io.github.synzkah.client.components;

import io.github.synzkah.client.utilities.MousePosition;
import lombok.Getter;
import lombok.Setter;

@Getter 
public abstract class SizedComponent extends Component {
	
	public Size size;

	public SizedComponent(Integer posX, Integer posY) {
		super(posX, posY);
	}
	
    public boolean isHovered() {
        if (MousePosition.getPosX() >= this.getPosX() && MousePosition.getPosY() >= this.getPosY() && MousePosition.getPosX() < this.getPosX() + this.size.getWidth()  && MousePosition.getPosY() < this.getPosY() + this.size.getHeight()) 
            return true;
        return false;
    }
	
    public boolean isPressed() {
        if (this.isActive() && this.isHovered()) 
            return true;
        return false;
    }
}
