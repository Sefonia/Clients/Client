package io.github.synzkah.client.components.backgrounds;

import io.github.synzkah.client.components.Size;
import io.github.synzkah.client.components.SizedComponent;

/**
 * @author Synzkah
 *	26 jul. 2018
 */

public abstract class Background extends SizedComponent  {
	
	public Background() {
		super(0, 0);
		
		this.size = Size.FULLSCREEN;
	}

	public Background(Integer posX, Integer posY) {
		super(posX, posY);
	}
	
	public abstract void draw();
}
