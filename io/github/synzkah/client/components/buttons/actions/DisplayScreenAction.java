package io.github.synzkah.client.components.buttons.actions;

import lombok.AllArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

@AllArgsConstructor
public class DisplayScreenAction extends Action {

    private GuiScreen target;

    @Override
    public void execute() {
    	Minecraft.getMinecraft().displayGuiScreen(target);
    }

}
