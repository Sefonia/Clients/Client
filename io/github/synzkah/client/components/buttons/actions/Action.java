package io.github.synzkah.client.components.buttons.actions;

/**
 * @author Synzkah
 *	25 jul. 2018
 */

public abstract class Action {
	
	public abstract void execute(); 

}
