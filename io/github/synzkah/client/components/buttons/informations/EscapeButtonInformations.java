package io.github.synzkah.client.components.buttons.informations;

import io.github.synzkah.client.components.texts.Text;
import io.github.synzkah.client.components.texts.TextSize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor 
public class EscapeButtonInformations extends Informations {
	
	private Integer posX; 
	private Integer posY;

	@Override
	public void draw() {
		
		/**
		 * Title
		 */

		String title = getButton().getTitle();
		
		/**
		 * Draw
		 */
		
		if(title != null) 
			new Text(title, posX, posY)
				.setSize(TextSize.EXTRA_SMALL)
				.setColor(this.getButton().getTitleColor())
				.draw();
	}
}