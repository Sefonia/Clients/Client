package io.github.synzkah.client.components.buttons.informations;

import io.github.synzkah.client.components.buttons.Button;
import lombok.Data;

/**
 * @author Synzkah
 *	26 jul. 2018
 */

@Data
public abstract class Informations {

	private Button button;
	
	public abstract void draw();
	
}