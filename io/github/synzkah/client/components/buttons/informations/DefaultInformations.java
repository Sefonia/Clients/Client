package io.github.synzkah.client.components.buttons.informations;

import io.github.synzkah.client.components.texts.Text;
import io.github.synzkah.client.utilities.Utilities;

/**
 * @author Synzkah
 *	26 jul. 2018
 */

public class DefaultInformations extends Informations {

	@Override
	public void draw() {
		
		/**
		 * Title
		 */

		String title = getButton().getTitle();
		
		/**
		 * Draw
		 */
		
		if(title != null) 
			new Text(title, this.getButton().getPosX() + (this.getButton().getSize().getWidth() - Utilities.getStringWidth(title)) / 2, this.getButton().getPosY() + (this.getButton().getSize().getHeight() - 8) / 2)
			.setColor(this.getButton().getTitleColor())
			.draw();
	}

}
