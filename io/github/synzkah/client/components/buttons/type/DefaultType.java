package io.github.synzkah.client.components.buttons.type;

import java.awt.Color;

import io.github.synzkah.client.components.backgrounds.ColoredBackground;
import io.github.synzkah.client.utilities.Colors;

/**
 * @author Synzkah
 *	26 jul. 2018
 */

public class DefaultType extends Type {

	@Override
	public void draw() {
    	Integer color = this.getButton().isActive() ? (this.getButton().isHovered() ? this.getButton().getColor().getLightColor() : this.getButton().getColor().getDarkColor()) : Color.LIGHT_GRAY.getRGB();
    	
        ColoredBackground rect = new ColoredBackground(this.getButton().getPosX(), this.getButton().getPosY());
        
        rect.withColor(new Colors(color));
        rect.withSize(this.getButton().getSize());
        rect.draw();
	}

}
