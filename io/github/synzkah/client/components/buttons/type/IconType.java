package io.github.synzkah.client.components.buttons.type;

import java.awt.Color;

import io.github.synzkah.client.components.Size;
import io.github.synzkah.client.components.backgrounds.ColoredBackground;
import io.github.synzkah.client.components.backgrounds.TexturedBackground;
import io.github.synzkah.client.resources.Resource;
import io.github.synzkah.client.utilities.Colors;
import io.github.synzkah.client.utilities.Utilities;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class IconType extends Type {
	
	//TODO: FadeInLeft gauche to droite w/ timer
	
	private Resource icon;

	@Override
	public void draw() {
		
		TexturedBackground texture = new TexturedBackground(this.getButton().getPosX(), this.getButton().getPosY());
		texture.withSize(this.getButton().getSize());
		texture.setResource(this.getIcon());
		
		Utilities.setGlColor(this.getButton().getColor().getLightColor());
		
		if(this.getButton().isHovered()) {
			
			new ColoredBackground(0, this.getButton().getPosY() - 1).withSize(new Size(34, 30)).withColor(new Colors(Color.WHITE)).withTransparency(0.5f).draw();
			Utilities.setGlColor(Color.BLACK.getRGB());
		}
		
		texture.draw();
	}
}