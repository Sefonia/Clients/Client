package io.github.synzkah.client.components.buttons.type;

import io.github.synzkah.client.components.buttons.Button;
import lombok.Data;

/**
 * @author Synzkah
 *	26 jul. 2018
 */

@Data
public abstract class Type {
	
	/**
	 * Button
	 */
	
	private Button button; 
	
	/**
	 * Draw
	 */
	
	public abstract void draw(); 

}
