package io.github.synzkah.client.components;

import io.github.synzkah.client.interfaces.Interface;
import lombok.Data;
import lombok.NonNull;

@Data
public class Size {
	
	public static Size FULLSCREEN = new Size(0,0);
	
	private Integer width; 
	private Integer height;
	
	public Size(Integer width, Integer height) {
		this.width = width;
		this.height = height;
	}
}
