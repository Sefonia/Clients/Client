package io.github.synzkah.client.settings;

import lombok.Data;

@Data
public class Settings {
	
	private boolean useBetterFonts = true;

}